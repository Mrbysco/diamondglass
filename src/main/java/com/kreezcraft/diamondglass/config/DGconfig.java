package com.kreezcraft.diamondglass.config;

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.fml.common.Mod;

import java.util.List;

@Mod.EventBusSubscriber
public class DGconfig {

    public static ForgeConfigSpec.IntValue rarity;

    public static ForgeConfigSpec.IntValue vein_size;

    public static ForgeConfigSpec.IntValue max_spawn_height;

      /*  public static ForgeConfigSpec.IntValue diamondsand_max_spawn_height_nether;

        public static ForgeConfigSpec.IntValue diamondsand_max_spawn_height_end;*/

    public static ForgeConfigSpec.BooleanValue enable_server_config_sync;
    public static ForgeConfigSpec.BooleanValue send_config_sync_packet;

    public static ForgeConfigSpec.BooleanValue overworld;
    public static ForgeConfigSpec.IntValue min_spawn_height;

    public static ForgeConfigSpec.BooleanValue biomeDebugMode;

    public static ForgeConfigSpec.ConfigValue<List<? extends String>> whitelist, blacklist;


    public static void init(ForgeConfigSpec.Builder builder) {

        Predicate<Object> validator = o -> o instanceof String;

        builder.comment("Note that almost all config changes require a server restart and/or a full minecraft restart. Dimensional Ore Configuration").push("dimensional_ore");


        overworld = builder
                .comment("Spawn diamondsand in the overworld? Default = true")
                .define("overworld", true);

        builder.pop();

        builder.comment("Rarity Configuration").push("Diamond Sand Overworld");


        rarity = builder
                .comment("Rarity of diamond sand veins. Default = 6")
                .defineInRange("rarity", 6, 0, 100);

        vein_size = builder
                .comment("Maximum vein size for an diamondsand Ore vein. For reference, diamonds have a max vein size of 8. Default = 7")
                .defineInRange("vein_size", 7, 0, 64);

        max_spawn_height = builder
                .comment("Maximum spawn height size for an diamondsand ore vein. Default = 16")
                .defineInRange("max_spawn_height", 64, 1, 255);

        min_spawn_height = builder
                .comment("Maximum spawn height size for an diamondsand ore vein. Default = 16")
                .defineInRange("min_spawn_height", 20, 1, 255);

        whitelist = builder
                .comment("List of biomes as strings separated with commas where this mod is allowed to spawn diamond sand ore. If this list is defined, normal generation no longer applies. Can be used with the blacklist. Use F3+h or a map mod to see biome names or turn on debug mode: example: minecraft:forest")
                .defineList("Whitelisted Biomes", Lists.newArrayList(), validator);

        blacklist = builder
                .comment("List of biomes as strings separated with commas where this mod is not allowed to spawn diamond sand ore. If this list is defined, normal generation no longer applies. Can be used with the whitelist. Use F3+h or a map mod to see biome names or turn on debug mode: example: minecraft:warm_ocean")
                .defineList("Blacklisted Biomes", Lists.newArrayList(), validator);

        biomeDebugMode=builder
                .comment("Toggle whether messages appear in console and logs about which biomes we have whitelisted or blacklisted or not")
                .define("Debug Biome Generation?",false);
        builder.pop();


        builder.push("network");

        enable_server_config_sync = builder
                .comment("Sync your config to the server upon joining? (Requires a Minecraft restart afterwards)")
                .define("enable_server_config_sync", true);

        send_config_sync_packet = builder
                .comment("Sends a packet that attempts to sync the client config file to the server?")
                .define("send_config_sync_packet", true);

        builder.pop();
    }

}

package com.kreezcraft.diamondglass.block;

import com.kreezcraft.diamondglass.InitBlocks;
import com.kreezcraft.diamondglass.DiamondGlass;
import net.minecraft.block.BlockState;
import net.minecraft.block.SoundType;
import net.minecraft.block.StainedGlassPaneBlock;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.DyeColor;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraftforge.common.ToolType;

public class DiamondGlassPane extends StainedGlassPaneBlock {
    public DiamondGlassPane(DyeColor color) {
        super(color, Properties.create(Material.GLASS, color).hardnessAndResistance(InitBlocks.glassHardness, 1200.0F).notSolid().sound(SoundType.GLASS));
        this.setRegistryName(new ResourceLocation(DiamondGlass.ModId, "diamondglasspane_" + color.toString()));
    }

    @Override
    public boolean isToolEffective(BlockState state, ToolType tool) {
        if (tool == ToolType.PICKAXE)
            return true;
        return false;
    }

    @Override
    public boolean canHarvestBlock(BlockState state, IBlockReader world, BlockPos pos, PlayerEntity player) {
        return true;
    }

    @Override
    public int getHarvestLevel(BlockState state) {
        return 3;
    }
}

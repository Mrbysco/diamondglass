package com.kreezcraft.diamondglass.world;

import com.kreezcraft.diamondglass.DiamondGlass;
import com.kreezcraft.diamondglass.InitBlocks;
import com.kreezcraft.diamondglass.config.DGconfig;
import com.mojang.serialization.Codec;
import net.java.games.input.DirectAndRawInputEnvironmentPlugin;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.WorldGenRegistries;
import net.minecraft.world.ISeedReader;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.OreFeature;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.Random;

@Mod.EventBusSubscriber(modid = DiamondGlass.ModId, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModFeatures {

    public static class DiamondGlassOre extends OreFeature {
        public DiamondGlassOre(Codec<OreFeatureConfig> codec) {
            super(codec);
        }

        //The correct way to limit the chance that the ore will generate for this chunk as "generate" is called once per chunk by world generator.
        @Override
        public boolean generate(ISeedReader reader, ChunkGenerator generator, Random rand, BlockPos pos, OreFeatureConfig config) {
            if (rand.nextInt(100) <= DGconfig.rarity.get())
                return super.generate(reader, generator, rand, pos, config);
            else
                return false;
        }
    }

    public static Feature DIAMOND_GLASS_ORE = new DiamondGlassOre(OreFeatureConfig.CODEC).setRegistryName("diamond_glass_ore");


    //This is ok to statically initialize as Forge doesnt enforce the registration of Configured Features
    public static final ConfiguredFeature<?, ?> DIAMOND_SAND_CONFIG = Registry.register(WorldGenRegistries.CONFIGURED_FEATURE, "diamond_sand",
            Feature.ORE.withConfiguration(
                    new OreFeatureConfig(
                            OreFeatureConfig.FillerBlockType.BASE_STONE_OVERWORLD,
                            InitBlocks.diamondSand.getDefaultState(), DGconfig.vein_size.get())
            ).range(DGconfig.max_spawn_height.get()).square().func_242731_b(DGconfig.min_spawn_height.get())
    );

    @SubscribeEvent
    public static void registerFeatures(RegistryEvent.Register<Feature<?>> event) {
        DiamondGlass.LOGGER.debug("Diamond Glass: Registering features...");
        event.getRegistry().register(DIAMOND_GLASS_ORE);
        DiamondGlass.LOGGER.info("Diamond Glass: Features registered!");
    }

    @SubscribeEvent
    public void onBiomeLoading(final BiomeLoadingEvent biome) {
        if (DGconfig.blacklist.get().size() > 0) {
            //blacklist in use
            if (DGconfig.blacklist.get().contains(biome.getName().toString())) {
                if (DGconfig.biomeDebugMode.get())
                    DiamondGlass.LOGGER.info(biome.getName().toString() + " is blacklisted.");
                return; // is blacklisted
            }
        }
        if (DGconfig.whitelist.get().size() > 0) {
            //whitelist in use
            if (DGconfig.whitelist.get().contains(biome.getName().toString()) == false) {
                if (DGconfig.biomeDebugMode.get())
                    DiamondGlass.LOGGER.info(biome.getName().toString() + " is not whitelisted.");
                return; //not in whitelist
            }
        }
        if (DGconfig.biomeDebugMode.get())
            DiamondGlass.LOGGER.info("Generating diamond sand ore for " + biome.getName().toString());
        if (biome.getCategory() == Biome.Category.NETHER || biome.getCategory() == Biome.Category.THEEND) return;
        if (DGconfig.overworld.get())
            biome.getGeneration().getFeatures(GenerationStage.Decoration.UNDERGROUND_ORES)
                    .add(() -> ModFeatures.DIAMOND_SAND_CONFIG);
    }
}
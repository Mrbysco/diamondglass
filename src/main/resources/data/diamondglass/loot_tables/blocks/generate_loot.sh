#!/usr/bin/bash
FORMAT="{
  \"type\": \"minecraft:block\",
  \"pools\": [
    {
	  \"name\": \"diamondglass:${1}\",
      \"rolls\": 1,
      \"entries\": [
        {
          \"type\": \"minecraft:item\",
          \"name\": \"diamondglass:${1}\"
        }
      ],
      \"conditions\": [
        {
          \"condition\": \"minecraft:survives_explosion\"
        }
      ]
    }
  ]
}"

echo "${FORMAT}" > "${1}.json"
